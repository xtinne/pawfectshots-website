import LocalizedStrings from 'react-localization';
import nl from '../locales/nl.json';
import en from '../locales/en.json';

const strings = new LocalizedStrings({ nl, en });

export const setActiveLanguage = language => {
	strings.setLanguage(language);
	return language;
};

export default strings;
