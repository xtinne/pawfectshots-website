import 'babel-polyfill';
import React from 'react';
import Intro from './intro/Intro';
import './home.css';

const Home = () => (
	<div className="app">
		<Intro />
	</div>
);

export default Home;
