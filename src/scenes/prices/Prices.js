import React from 'react';
import Title from '../../components/title/Title';
import List from '../../components/list/List';
import './prices.css';

const imgStarter = require('../../assets/images/portfolio/img10.jpg');
const imgBasis = require('../../assets/images/portfolio/img18.jpg');
const imgLuxe = require('../../assets/images/portfolio/img01.jpg');
const imgPup = require('../../assets/images/portfolio/img02.jpg');
const imgNest = require('../../assets/images/portfolio/img42.jpg');
const imgPortfolio = require('../../assets/images/portfolio/img22.jpg');

const Prices = () => {
    const list = [
        {
            id: 1,
            title: 'Starter',
            price: '35 euro',
            items: [
                "10 foto's op webformaat met logo",
                "Cadeautje voor de hond",
            ],
            img: imgStarter,
        },
        {
            id: 2,
            title: 'Basis',
            price: '50 euro',
            items: [
                "15 foto's op webformaat met logo",
                "2 foto's op hoge resolutie zonder logo",
                "Cadeautje voor de hond",
            ],
            img: imgBasis,
        },
        {
            id: 3,
            title: 'Luxe',
            price: '80 euro',
            items: [
                "15 foto's op webformaat met logo",
                "15 foto's op hoge resolutie zonder logo",
                "Cadeautje voor de hond",
            ],
            img: imgLuxe,
        },
        // {
        //     id: 4,
        //     title: 'Van pup naar trouwe vriend',
        //     price: "120 of 140 euro",
        //     items: [
        //         "3 of 4 fotosessies gedurende het eerste levensjaar",
        //         "Per sessie 10 foto's op webformaat met logo",
        //         "Per sessie 1 foto op hoge resolutie zonder logo",
        //         "1 fotoprint op canvas 20x30 OF 1 fotoboek 15x21 26blz",
        //         "Korting op de verplaatsingskosten",
        //         "Cadeautje voor de pup bij de eerste en laatste sessie",
        //     ],
        //     img: imgPup,
        // },
        {
            id: 5,
            title: 'Nestshoot',
            price: "vanaf 50 euro",
            items: [
                "Voor fokkers of toekomstige pupeigenaars",
                "Van eenmalige tot wekelijkse fotosessies tijdens de eerste levensweken",
                "Per sessie 1 foto per pup inbegrepen, op webformaat met logo",
                "Mogelijkheid tot bijbestellen",
            ],
            img: imgNest,
        },
        {
            id: 6,
            title: 'Portfolioshoot',
            items: [
                "Voor honden met een unieke uitstraling, speciale rassen, ...",
                "Sessie zal plaatsvinden in de buurt van Oostmalle",
                "3 foto's op webformaat met logo",
            ],
            img: imgPortfolio,
        },
    ];

    return (
        <div className="prices">
            <Title title="Pakketten" className="h1" />
            <div className="small-reading">
            <p>De prijzen in de pakketten gelden per hond, dit zowel met als zonder baas.</p>
            <p>Vanaf 5 honden geldt er een korting van -25% op alle pakketten, vanaf 10 honden is dit -50%.</p>
            <p>Alle prijzen zijn exclusief verplaatsingskosten: €0,30 per km vanuit Oostmalle.</p>
            <p>Bij elk pakket is er de mogelijkheid om extra foto's bij te bestellen. Deze prijslijst kan u verder in dit document terugvinden.</p>
            <p>Enkel de foto's met logo mogen gebruikt worden voor sociale media. Foto's zonder logo zijn uitsluitend voor eigen gebruik.</p>
            </div>
            <List list={list} /> 
        </div>
    );
};

export default Prices;