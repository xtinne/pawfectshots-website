import React, { PureComponent } from 'react';
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';
import './portfolio.css';

class Portfolio extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentImage: 0,
            lightboxIsOpen: false,
        };
    }

    openLightbox = (_, obj) => {
        this.setState({
            currentImage: obj.index,
            lightboxIsOpen: true,
        });
    }

    closeLightbox = () => {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false,
        });
    }

    gotoPrevious = () => {
        this.setState({
            currentImage: this.state.currentImage - 1,
        });
    }

    gotoNext = () => {
        this.setState({
            currentImage: this.state.currentImage + 1,
        });
    }

    render() {
        // console.log(images);
        return (
            <div className="portfolio">
                <Gallery photos={PHOTO_SET} onClick={this.openLightbox} />
                <Lightbox images={PHOTO_SET}
                    onClose={this.closeLightbox}
                    onClickPrev={this.gotoPrevious}
                    onClickNext={this.gotoNext}
                    currentImage={this.state.currentImage}
                    isOpen={this.state.lightboxIsOpen}
                />
            </div>
        );
    }
}

export default Portfolio;


const img1 = require('../../assets/images/portfolio/img01.jpg');
const img2 = require('../../assets/images/portfolio/img02.jpg');
const img3 = require('../../assets/images/portfolio/img03.jpg');
const img4 = require('../../assets/images/portfolio/img04.jpg');
const img5 = require('../../assets/images/portfolio/img05.jpg');
const img6 = require('../../assets/images/portfolio/img06.jpg');
const img7 = require('../../assets/images/portfolio/img07.jpg');
const img8 = require('../../assets/images/portfolio/img08.jpg');
const img9 = require('../../assets/images/portfolio/img09.jpg');
const img10 = require('../../assets/images/portfolio/img10.jpg');
const img11 = require('../../assets/images/portfolio/img11.jpg');
const img12 = require('../../assets/images/portfolio/img12.jpg');
const img13 = require('../../assets/images/portfolio/img13.jpg');
const img14 = require('../../assets/images/portfolio/img14.jpg');
const img15 = require('../../assets/images/portfolio/img15.jpg');
const img16 = require('../../assets/images/portfolio/img16.jpg');
const img17 = require('../../assets/images/portfolio/img17.jpg');
const img18 = require('../../assets/images/portfolio/img18.jpg');
const img19 = require('../../assets/images/portfolio/img19.jpg');
const img20 = require('../../assets/images/portfolio/img20.jpg');
const img21 = require('../../assets/images/portfolio/img21.jpg');
const img22 = require('../../assets/images/portfolio/img22.jpg');
const img23 = require('../../assets/images/portfolio/img23.jpg');
const img24 = require('../../assets/images/portfolio/img24.jpg');
const img25 = require('../../assets/images/portfolio/img25.jpg');
const img26 = require('../../assets/images/portfolio/img26.jpg');
const img27 = require('../../assets/images/portfolio/img27.jpg');
const img28 = require('../../assets/images/portfolio/img28.jpg');
const img29 = require('../../assets/images/portfolio/img29.jpg');
const img30 = require('../../assets/images/portfolio/img30.jpg');
const img31 = require('../../assets/images/portfolio/img31.jpg');
const img32 = require('../../assets/images/portfolio/img32.jpg');
const img33 = require('../../assets/images/portfolio/img33.jpg');
const img34 = require('../../assets/images/portfolio/img34.jpg');
const img35 = require('../../assets/images/portfolio/img35.jpg');
const img36 = require('../../assets/images/portfolio/img36.jpg');
const img37 = require('../../assets/images/portfolio/img37.jpg');
const img38 = require('../../assets/images/portfolio/img38.jpg');
const img39 = require('../../assets/images/portfolio/img39.jpg');
const img40 = require('../../assets/images/portfolio/img40.jpg');
const img41 = require('../../assets/images/portfolio/img41.jpg');
const img42 = require('../../assets/images/portfolio/img42.jpg');
const img43 = require('../../assets/images/portfolio/img43.jpg');
const img44 = require('../../assets/images/portfolio/img44.jpg');
const img45 = require('../../assets/images/portfolio/img45.jpg');
const img46 = require('../../assets/images/portfolio/img46.jpg');
const img47 = require('../../assets/images/portfolio/img47.jpg');
const img48 = require('../../assets/images/portfolio/img48.jpg');
const img49 = require('../../assets/images/portfolio/img49.jpg');
const img50 = require('../../assets/images/portfolio/img50.jpg');
const img51 = require('../../assets/images/portfolio/img51.jpg');
const img52 = require('../../assets/images/portfolio/img52.jpg');
const img53 = require('../../assets/images/portfolio/img53.jpg');
const img54 = require('../../assets/images/portfolio/img54.jpg');
const img55 = require('../../assets/images/portfolio/img55.jpg');
const img56 = require('../../assets/images/portfolio/img56.jpg');
const img57 = require('../../assets/images/portfolio/img57.jpg');
const img58 = require('../../assets/images/portfolio/img58.jpg');
const img59 = require('../../assets/images/portfolio/img59.jpg');
const img60 = require('../../assets/images/portfolio/img60.jpg');
const img61 = require('../../assets/images/portfolio/img61.jpg');
const img62 = require('../../assets/images/portfolio/img62.jpg');
const img63 = require('../../assets/images/portfolio/img63.jpg');
const img64 = require('../../assets/images/portfolio/img64.jpg');
const img65 = require('../../assets/images/portfolio/img65.jpg');
const img66 = require('../../assets/images/portfolio/img66.jpg');
const img67 = require('../../assets/images/portfolio/img67.jpg');
const img68 = require('../../assets/images/portfolio/img68.jpg');
const img69 = require('../../assets/images/portfolio/img69.jpg');
const img70 = require('../../assets/images/portfolio/img70.jpg');
const img71 = require('../../assets/images/portfolio/img71.jpg');
const img72 = require('../../assets/images/portfolio/img72.jpg');
const img73 = require('../../assets/images/portfolio/img73.jpg');
const img74 = require('../../assets/images/portfolio/img74.jpg');
const img75 = require('../../assets/images/portfolio/img75.jpg');
const img76 = require('../../assets/images/portfolio/img76.jpg');
const img77 = require('../../assets/images/portfolio/img77.jpg');
const img78 = require('../../assets/images/portfolio/img78.jpg');
const img79 = require('../../assets/images/portfolio/img79.jpg');
const img80 = require('../../assets/images/portfolio/img80.jpg');
const img81 = require('../../assets/images/portfolio/img81.jpg');
const img82 = require('../../assets/images/portfolio/img82.jpg');
const img83 = require('../../assets/images/portfolio/img83.jpg');
const img84 = require('../../assets/images/portfolio/img84.jpg');
const img85 = require('../../assets/images/portfolio/img85.jpg');
const img86 = require('../../assets/images/portfolio/img86.jpg');
const img87 = require('../../assets/images/portfolio/img87.jpg');
const img88 = require('../../assets/images/portfolio/img88.jpg');
const img89 = require('../../assets/images/portfolio/img89.jpg');
const img90 = require('../../assets/images/portfolio/img90.jpg');
const img91 = require('../../assets/images/portfolio/img91.jpg');
const img92 = require('../../assets/images/portfolio/img92.jpg');
const img93 = require('../../assets/images/portfolio/img93.jpg');
const img94 = require('../../assets/images/portfolio/img94.jpg');

// function importAll(r) {
//     return r.keys().map(r);
//   }
  
// const images = importAll(require.context('../../assets/images/portfolio', false, /\.(png|jpe?g|svg)$/));

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }

const PHOTO_SET = shuffle([
    {
        src: img1,
        width: 4.5,
        height: 3,
    },
    {
        src: img2,
        width: 4.5,
        height: 3,
    },
    {
        src: img3,
        width: 4.5,
        height: 3,
    },
    {
        src: img4,
        width: 4.5,
        height: 3,
    },
    {
        src: img5,
        width: 4.5,
        height: 3,
    },
    {
        src: img6,
        width: 4.5,
        height: 3,
    },
    {
        src: img7,
        width: 3,
        height: 4.5,
    },
    {
        src: img8,
        width: 3,
        height: 4.5,
    },
    {
        src: img9,
        width: 4.5,
        height: 3,
    },
    {
        src: img10,
        width: 4.5,
        height: 3,
    },
    {
        src: img11,
        width: 4.5,
        height: 3,
    },
    {
        src: img12,
        width: 3,
        height: 4.5,
    },
    {
        src: img13,
        width: 4.5,
        height: 3,
    },
    {
        src: img14,
        width: 4.5,
        height: 3,
    },
    {
        src: img15,
        width: 4.5,
        height: 3,
    },
    {
        src: img16,
        width: 3,
        height: 4.5,
    },
    {
        src: img17,
        width: 4.5,
        height: 3,
    },
    {
        src: img18,
        width: 4.5,
        height: 3,
    },
    {
        src: img19,
        width: 4.5,
        height: 3,
    },
    {
        src: img20,
        width: 3,
        height: 4.5,
    },
    {
        src: img21,
        width: 4.5,
        height: 3,
    },
    {
        src: img22,
        width: 4.5,
        height: 3,
    },
    {
        src: img23,
        width: 4.5,
        height: 3,
    },
    {
        src: img24,
        width: 4.5,
        height: 3,
    },
    {
        src: img25,
        width: 4.5,
        height: 3,
    },
    {
        src: img26,
        width: 3,
        height: 4.5,
    },
    {
        src: img27,
        width: 4.5,
        height: 3,
    },
    {
        src: img28,
        width: 3,
        height: 4.5,
    },
    {
        src: img29,
        width: 3,
        height: 4.5,
    },
    {
        src: img30,
        width: 3,
        height: 4.5,
    },
    {
        src: img31,
        width: 3,
        height: 4.5,
    },
    {
        src: img32,
        width: 4.5,
        height: 3,
    },
    {
        src: img33,
        width: 4.5,
        height: 3,
    },
    {
        src: img34,
        width: 3,
        height: 4.5,
    },
    {
        src: img35,
        width: 4.5,
        height: 3,
    },
    {
        src: img36,
        width: 4.5,
        height: 3,
    },
    {
        src: img37,
        width: 3,
        height: 4.5,
    },
    {
        src: img38,
        width: 4.5,
        height: 3,
    },
    {
        src: img39,
        width: 4.5,
        height: 3,
    },
    {
        src: img40,
        width: 4.5,
        height: 3,
    },
    {
        src: img41,
        width: 4.5,
        height: 3,
    },
    {
        src: img42,
        width: 4.5,
        height: 3,
    },
    {
        src: img43,
        width: 4.5,
        height: 3,
    },
    {
        src: img44,
        width: 4.5,
        height: 3,
    },
    {
        src: img45,
        width: 4.5,
        height: 3,
    },
    {
        src: img46,
        width: 4.5,
        height: 3,
    },
    {
        src: img47,
        width: 4.5,
        height: 3,
    },
    {
        src: img48,
        width: 4.5,
        height: 3,
    },
    {
        src: img49,
        width: 3,
        height: 4.5,
    },
    {
        src: img50,
        width: 4.5,
        height: 3,
    },
    {
        src: img51,
        width: 3,
        height: 4.5,
    },
    {
        src: img52,
        width: 4.5,
        height: 3,
    },
    {
        src: img53,
        width: 4.5,
        height: 3,
    },
    {
        src: img54,
        width: 4.5,
        height: 3,
    },
    {
        src: img55,
        width: 4.5,
        height: 3,
    },
    {
        src: img56,
        width: 4.5,
        height: 3,
    },
    {
        src: img57,
        width: 4.5,
        height: 3,
    },
    {
        src: img58,
        width: 4.5,
        height: 3,
    },
    {
        src: img59,
        width: 4.5,
        height: 3,
    },
    {
        src: img60,
        width: 4.5,
        height: 3,
    },
    {
        src: img61,
        width: 4.5,
        height: 3,
    },
    {
        src: img62,
        width: 4.5,
        height: 3,
    },
    {
        src: img63,
        width: 4.5,
        height: 3,
    },
    {
        src: img64,
        width: 4.5,
        height: 3,
    },
    {
        src: img65,
        width: 4.5,
        height: 3,
    },
    {
        src: img66,
        width: 4.5,
        height: 3,
    },
    {
        src: img67,
        width: 4.5,
        height: 3,
    },
    {
        src: img68,
        width: 3,
        height: 4.5,
    },
    {
        src: img69,
        width: 4.5,
        height: 3,
    },
    {
        src: img70,
        width: 4.5,
        height: 3,
    },
    {
        src: img71,
        width: 4.5,
        height: 3,
    },
    {
        src: img72,
        width: 3,
        height: 3,
    },
    {
        src: img73,
        width: 3,
        height: 3,
    },
    {
        src: img74,
        width: 3,
        height: 3,
    },
    {
        src: img75,
        width: 3,
        height: 3,
    },
    {
        src: img76,
        width: 3,
        height: 3,
    },
    {
        src: img77,
        width: 4.5,
        height: 3,
    },
    {
        src: img78,
        width: 4.5,
        height: 3,
    },
    {
        src: img79,
        width: 4.5,
        height: 3,
    },
    {
        src: img80,
        width: 4.5,
        height: 3,
    },
    {
        src: img81,
        width: 4.5,
        height: 3,
    },
    {
        src: img82,
        width: 4.5,
        height: 3,
    },
    {
        src: img83,
        width: 3,
        height: 4.5,
    },
    {
        src: img84,
        width: 3,
        height: 4.5,
    },
    {
        src: img85,
        width: 3,
        height: 4.5,
    },
    {
        src: img86,
        width: 3,
        height: 3,
    },
    {
        src: img87,
        width: 3,
        height: 3,
    },
    {
        src: img88,
        width: 3,
        height: 3,
    },
    {
        src: img89,
        width: 3,
        height: 3,
    },
    {
        src: img90,
        width: 3,
        height: 3,
    },
    {
        src: img91,
        width: 3,
        height: 3,
    },
    {
        src: img92,
        width: 3,
        height: 3,
    },
    {
        src: img93,
        width: 3,
        height: 3,
    },
    {
        src: img94,
        width: 3,
        height: 3,
    },
]);