import React from 'react';
import './footer.css';

const Footer = () => (
    <div className="footer">
        <p>&copy; Pawfect Shots | BTW BE 0701.915.655 | <a href="mailto:hallo@pawfectshots.be">hallo@pawfectshots.be</a> | <a href="/algemene-voorwaarden">Algemene voorwaarden</a></p>

        <p className="social-icons">
            <a href="https://www.facebook.com/pawfectshots" target="_blank" rel="noopener noreferrer" className="social-icon"> <i className="fa fa-facebook"></i></a>
            <a href="https://www.instagram.com/pawfectshots" target="_blank" rel="noopener noreferrer" className="social-icon"> <i className="fa fa-instagram"></i></a>
        </p>
    </div>
);

export default Footer;
