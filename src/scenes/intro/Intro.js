import React from 'react';
import Carousel from 'nuka-carousel';
import './intro.css';

const img1 = require('../../assets/images/home-1.jpg');
const img2 = require('../../assets/images/home-2.jpg');
const img3 = require('../../assets/images/home-3.jpg');
const img4 = require('../../assets/images/home-4.jpg');

const Intro = () => (
    <div className="intro">
        <Carousel autoplay={true} pauseOnHover={true}> 
            <img src={img1} alt=""/>
            <img src={img2} alt=""/>
            <img src={img3} alt=""/>
            <img src={img4} alt=""/>
        </Carousel>
    </div>
);

export default Intro;