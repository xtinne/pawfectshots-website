import React from 'react';
import Title from '../../components/title/Title';
import List from '../../components/list/List';
import './backorder.css';

const imgDigitaal = require('../../assets/images/portfolio/img60.jpg');
const imgPrint = require('../../assets/images/portfolio/img03.jpg');
const imgWanddeco = require('../../assets/images/portfolio/img25.jpg');
const imgPup = require('../../assets/images/portfolio/img40.jpg');

const Prices = () => {
    const list = [
        {
            id: 1,
            title: 'Digitale bestanden',
            items: [
                "Foto op webformaat met logo: 3 euro",
                "Foto op hoge resolutie zonder logo: 8 euro",
            ],
            img: imgDigitaal,
        },
        {
            id: 2,
            title: 'Fotoprints',
            items: [
                "10x15: 3 euro",
                "13x18: 4,5 euro",
                "20x30: 7 euro",
                "30x45: 9 euro",
            ],
            img: imgPrint,
        },
        {
            id: 3,
            title: 'Wanddecoratie',
            items: [
                "Prijs afhankelijk van gewenste grootte en gewenst materiaal",
                "Mogelijke groottes: 10x15, 20x30, 40x60, 50x70, ...",
                "Mogelijke materialen: fotoposter, canvas, alu-dibond, acrylglas, ...",
            ],
            img: imgWanddeco,
        },
        {
            id: 4,
            title: 'Nabestellingen nestshoot',
            items: [
                "Voor fokkers of nieuwe pupeigenaars",
                "Fotoboekje 13x15: 20 euro",
                "Foto met logo: 4 euro (Bij aankoop boekje: 3 euro)",
                "Foto zonder logo: 10 euro (Bij aankoop boekje: 8 euro)",
            ],
            img: imgPup,
        },
    ];

    return (
        <div className="prices">
            <Title title="Nabestellingen" className="h1" />
            <div className="small-reading">
            <p>Alle prijzen zijn exclusief verzendkosten.</p>
            <p>Digitale foto's worden verzonden via WeTransfer, tenzij anders afgesproken.</p>
            <p>Enkel de foto's met logo mogen gebruikt worden voor sociale media ed. Foto's zonder logo zijn uitsluitend voor eigen gebruik.</p>
            <p>Vind je in deze lijst niet helemaal wat je graag wilt? Laat me even weten wat jouw wensen zijn, zodat ik kan kijken of het mogelijk is!</p>
            </div>
            <List list={list} />
        </div>
    );
};

export default Prices;