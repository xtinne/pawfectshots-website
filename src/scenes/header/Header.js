import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { strings } from '../../utils';
import './header.css';

const logo = require('../../assets/images/logo.png');

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {
			navOpen: false,
		};
	}

	handleClick = () => {
		this.setState(state => ({ navOpen: !state.navOpen }));
	}

	render() {
		return (
			<div className="header">
				<div className="mobile__logo">
					<a href="/"><img src={logo} alt="Logo" /></a>
				</div>
				<a className="icon__hamburger" onClick={this.handleClick}><i className={this.state.navOpen ? 'icon-close' : 'icon-hamburger'}></i></a>
				<div className={`header__inner ${this.state.navOpen ? 'header__inner--active' : ''}`}>
					<div className="header__logo">
						<a href="/"><img src={logo} alt="Logo" /></a>
					</div>
					<div className="header__nav">
						<ul>
							<li><NavLink activeClassName='nav--active' onClick={this.handleClick} to="/" exact>{strings.NAV_HOME}</NavLink></li>
							<li><NavLink activeClassName='nav--active' onClick={this.handleClick} to="/about" exact>{strings.NAV_ABOUT}</NavLink></li>
							<li><NavLink activeClassName='nav--active' onClick={this.handleClick} to="/portfolio" exact>{strings.NAV_PORTFOLIO}</NavLink></li>
							<li><NavLink activeClassName='nav--active' onClick={this.handleClick} to="/contact" exact>{strings.NAV_CONTACT}</NavLink></li>
						</ul>
					</div>
				</div>
			</div >
		);
	}
}

export default Header;
