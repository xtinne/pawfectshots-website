import { useState, useCallback, useMemo } from 'react'

const useInput = (initial = '') => {
  const [value, setValue] = useState(initial)
  const [error, setError] = useState('')

  const onChange = useCallback(e => {
    setValue(e.target.value)
    setError('')
  }, [])
  const clear = useCallback(() => {
    setValue('')
    setError('')
  }, [])

  return useMemo(
    () => ({
      value,
      setValue,
      hasValue: value !== undefined && value !== null && value.trim() !== '',
      clear,
      onChange,
      eventBind: {
        onChange,
        value
      },
      valueBind: {
        onChange: setValue,
        value
      },
      error,
      setError
    }),
    [clear, onChange, value, error]
  )
}

export default useInput