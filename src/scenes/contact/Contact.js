import React, { useCallback, useState } from 'react'
import './contact.css';
import axios from 'axios'
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3'
import useInput from './useInput'
import Title from '../../components/title/Title';

const EMAIL_REGEX = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+.)+[^<>()[\].,;:\s@"]{2,})$/i

const Contact = () => {
  const name = useInput('')
  const email = useInput('')
  const subject = useInput('')
  const message = useInput('')
  const [sendResult, setSendResult] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const { executeRecaptcha } = useGoogleReCaptcha()

  const onSubmit = useCallback(async event => {
    event.preventDefault()
    setIsLoading(true)
    setSendResult('')

    let valid = true
    if (!name.hasValue) {
      name.setError('Gelieve een naam in te geven.')
      valid = false
    }
    if (!email.hasValue || !EMAIL_REGEX.test(email.value)) {
      email.setError('Gelieve een geldig e-mailadres in te geven.')
      valid = false
    }
    if (!subject.hasValue) {
      subject.setError('Gelieve een onderwerp in te geven.')
      valid = false
    }
    if (!message.hasValue) {
      message.setError('Gelieve een bericht in te geven.')
      valid = false
    }

    if (valid) {
      try {
        console.log(valid);
        const token = await executeRecaptcha('contact')

        console.log(token);
        await axios.post('/mail', {
          token,
          from: email.value,
          subject: subject.value,
          title: subject.value,
          preheader: subject.value,
          content:
          `<h2 style='text-align: center'>Bedankt voor uw bericht! Wij nemen zo spoedig mogelijk contact op.</h2>` +
          `<h4 style='margin-bottom: 10px; font-weight: 800'>Uw bericht</h4>` +
          `<p>${message.value}</p>`
        })

        name.clear()
        email.clear()
        subject.clear()
        message.clear()
        setSendResult('Succesvol verzonden!')
      } catch (error) {
        setSendResult('Oeps... Er ging iets mis.')
      }
    }
    setIsLoading(false)
  }, [name, email, subject, message, executeRecaptcha])

  return (
    <div className='contact'>
      <Title title="Contacteer ons" className="h1" />
      <p>Wil je graag meer informatie over een fotosessie? Of heb je een andere vraag?
        Aarzel niet om ons te contacteren! We voorzien je dan zo snel mogelijk van een antwoord.</p>
        <form onSubmit={onSubmit}>
              <input {...name.eventBind} type='text' placeholder={'Naam'} />
              <p className="form__error">{name.error}</p>
              <input {...email.eventBind} type='email' placeholder={'E-mailadres'} />
              <p className="form__error">{email.error}</p>
            <input {...subject.eventBind} type='text' placeholder={'Onderwerp'} />
            <p className="form__error">{subject.error}</p>
            <textarea {...message.eventBind} rows='5' placeholder={'Bericht'} />
            <p className="form__error">{message.error}</p>
          <input type='hidden' name='recaptcha_response' id='recaptchaResponse' />
            <button className="btn" type='submit'>{ isLoading ? '...' : 'Verzenden'}</button>
            <p className="form__error">{sendResult}</p>
        </form>
    </div>
  )
}

export default Contact