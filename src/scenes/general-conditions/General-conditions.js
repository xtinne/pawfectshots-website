import React from 'react';
import Title from '../../components/title/Title';
import List from '../../components/list/List';
import './general-conditions.css';

const GeneralConditions = () => {
    const list = [
        {
            id: 1,
            title: 'Prijzen',
            items: [
                "Prijzen in de pakketten gelden per hond, dit zowel met als zonder baas.",
                "Vanaf 5 honden geldt er een korting van -25%. Vanaf 10 honden is dit -50%.",
                "Alle prijzen zijn exclusief verzendkosten en verplaatsingskosten (30 cent per kilometer vanuit Oostmalle).",
                "Alle betalingen verlopen via overschrijving.",
            ],
        },
        {
            id: 2,
            title: 'Fotosessie',
            items: [
                "Het totaalbedrag wordt ten laatste voldaan op de dag van de fotosessie.",
                "Een fotosessie kan tot 5 dagen op voorhand kosteloos worden geannuleerd. Nadien wordt er 30% van het totaalbedrag in rekening gebracht.",
                "Bij slecht weer wordt de fotosessie in overleg kosteloos verplaatst.",
            ],
        },
        {
            id: 3,
            title: 'Na de fotosessie',
            items: [
                "Na de fotosessie krijg je zo snel mogelijk 1 foto met logo doorgestuurd als voorproefje.",
                "Het logo wordt geplaatst op de foto, naast het model. Dit om het afknippen van het logo te voorkomen.",
                "Binnen 1 a 2 weken volgen de overige foto's met groot watermerk, waaruit je zelf de mooiste foto's mag kiezen die afgeleverd zullen worden.",
                "Deze selectie moeten doorgegeven worden binnen de 3 weken. Indien dit niet gebeurd, stel ik de selectie zelf samen.",
                "De digitale bestanden worden 2 weken na het kiezen van de foto's afgeleverd via WeTransfer.",
                "Bij elk pakket is er de mogelijkheid om foto's (zowel met als zonder logo) en afdrukken bij te bestellen.",
                "Enkel de foto's met logo mogen gebruikt worden voor social media.",
                "Foto's zonder logo zijn uitsluitend voor eigen gebruik.",
                "Alle gemaakte foto's mogen gebruikt worden voor mijn portfolio. Indien je dit niet wilt, kan je dit op voorhand melden."
            ],
        },
        {
            id: 4,
            title: 'Persoonsgegevens',
            items: [
                "Alle persoonsgegevens worden vertrouwelijk behandeld en nooit doorgegeven aan derden, tenzij dit noodzakelijk is (bv boekhouding).",
                "Voor het vlot uitvoeren van onze diensten verwerken wij naam, adres, e-mailadres, telefoonnummer en bankrekeningnummer.",
                "Je hebt steeds het recht om deze gegevens in te kijken, aan te passen of te laten verwijderen.",
                "Indien je ervoor kiest om je gegevens te laten verwijderen, verwijderen wij alle gegevens die wij niet nodig hebben voor officiële doeleinden (bv facturatie).",
            ],
        },
    ];

    return (
        <div className="general-conditions">
            <Title title="Algemene Voorwaarden" className="h1" />
            <div className="small-reading">
                <p>Bij het boeken van een fotosessie gaat men automatisch akkoord met volgende algemene voorwaarden.</p>
            </div>
            <List list={list} />
        </div>
    );
};

export default GeneralConditions;