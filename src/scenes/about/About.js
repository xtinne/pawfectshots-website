import React from 'react';
import './about.css';
import AboutBlock from '../../components/about-block/About-block';

const img1 = require('../../assets/images/tinne-1.jpg');
const img2 = require('../../assets/images/tinne-2.jpg');
const img3 = require('../../assets/images/tinne-3.jpg');

const About = () => (
    <div className="about">
        <AboutBlock img={img1} title="Korte voorstelling" text={
            "Hallo! Mijn naam is Tinne, geboren in 1996 en wonend in Malle. Naast mijn fulltime job als front-end developer, " +
        "heb ik een grote liefde voor honden en fotografie. " +
        "Door deze twee grote liefdes te combineren, startte ik enkele jaren geleden als hondenfotograaf."
    }
        />

        <AboutBlock img={img2} right={true} title="Waarom fotografie?" text="Voor mij zijn foto’s altijd belangrijk geweest. 
            Ze hebben een onbeschrijflijke waarde en dragen steeds een emotie met zich mee. Soms voel je geluk, soms voel je verdriet door een intens gemis.
            Elke foto heeft zijn eigen verhaal en daarom ook zijn eigen, unieke gevoel.
            Dat is voor mij de grootste reden waarom ik verliefd werd op fotografie.
            Omdat simpelweg elke foto uniek is."
        />

        <AboutBlock img={img3} title="Waarom Pawfect Shots?" text="Tijdens een fotoreportage wil ik jullie enorm graag de liefde voor 
            een foto meegeven. Het is belangrijk dat je zelf geniet, zonder aan de camera te denken. 
            Gewoon jezelf zijn, zodat de emoties de kans krijgen om naar boven te komen. 
            Om als slot een prachtige reeks foto's te ontvangen waarbij jullie weken, maanden of zelfs jaren later nog eens 
            naar de foto’s kunnen kijken en alle bijhorende emoties tot in het diepste van jullie hart opnieuw kunnen voelen."
        />
    </div>
);

export default About;