import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { setActiveLanguage } from './utils';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3'
import Home from './scenes/Home';
import About from './scenes/about/About';
import Header from './scenes/header/Header';
import Footer from './scenes/footer/Footer';
import Portfolio from './scenes/portfolio/Portfolio';
import Prices from './scenes/prices/Prices';
import Backorder from './scenes/backorder/Backorder';
import GeneralConditions from './scenes/general-conditions/General-conditions';
import Contact from './scenes/contact/Contact';

class App extends Component {
	constructor(props) {
		super(props);
		// if (window.location.pathname === '/nl') {
		setActiveLanguage('nl');
		// } else {
		// 	setActiveLanguage('en');
		// }
	}

	render() {
		return (
							<GoogleReCaptchaProvider reCaptchaKey='6LcTKsMUAAAAAIkD27KkoV3azNqi6GiTx8vxavFI'>
			<BrowserRouter>
				<div>
					<Header />
					<Switch>
						{/* <Route path="/en" component={Home} exact /> */}
						{/* <Route path="/nl" component={Home} exact /> */}
						<Route path="/" component={Home} exact />
						<Route path="/about" component={About} exact />
						<Route path="/portfolio" component={Portfolio} exact />
						{/* <Route path="/prijslijst" component={Prices} exact /> */}
						{/* <Route path="/nabestellingen" component={Backorder} exact /> */}
						<Route path="/algemene-voorwaarden" component={GeneralConditions} exact />
						<Route path="/contact" exact>
								<Contact />
						</Route>
						<Redirect to="/" />
					</Switch>
					<Footer />
				</div>
			</BrowserRouter>
							</GoogleReCaptchaProvider>
		);
	}
}

export default App;
