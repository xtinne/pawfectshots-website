import { Network } from '../utils';

export const register = body => {
	return Network.post('/users/register', body);
};
