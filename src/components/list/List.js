import React from 'react';
import './list.css';

const List = props => (
	<div className="list" >
		{props.list.map(listItem =>
			<div className="list__item">
				<div className="item__text" style={!listItem.img ? {width:'100%'} : {}} >
					<h2 className="h2">{listItem.title}{listItem.price && ` - ${listItem.price}`}</h2>
					<ul>
						{listItem.items.map(item =>
							<li>{item}</li>
						)}
					</ul>
				</div>
				{listItem.img && 
					<div className="item__img">
						<img src={listItem.img}  alt=""/>
					</div>
				}
				
			</div>
		)}
	</div >
);

export default List;
