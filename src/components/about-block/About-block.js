import React from 'react';
import './about-block.css';
import Title from '../title/Title';

const AboutBlock = props => (
	<div className={`about__block ${props.right ? 'about__block--reverse' : ''}`} >
		<div className="about__image"><img src={props.img} alt="" /></div>
		<div className="about__text">
			<div className="text__title">
				<Title title={props.title} className="h2" />
			</div>
			<p>{props.text}</p>
		</div>
	</div >
);

export default AboutBlock;
