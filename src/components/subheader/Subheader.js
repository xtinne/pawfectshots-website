import React from 'react';
import moment from 'moment';
import 'moment/locale/nl';
import 'moment/locale/en-gb';
import { Button } from '../index';
import { strings, setActiveLanguage } from '../../utils';
import './subheader.css';

const logo = require('../../assets/images/logo.png');

const Subheader = props => {
	const isEnglish = strings.getLanguage() === 'en';

	return (
		<div className="subheader">
			<div>
				<Button
					theme={isEnglish ? 'primary' : 'inverse'}
					onClickCallback={() => {
						moment.locale('en-gb');
						setActiveLanguage('en');
						props.history.replace('en');
					}}
				>
					EN
				</Button>
				<Button
					theme={isEnglish ? 'inverse' : 'primary'}
					onClickCallback={() => {
						moment.locale('nl');
						setActiveLanguage('nl');
						props.history.replace('nl');
					}}
				>
					NL
				</Button>
			</div>
		</div>
	);
};

export default Subheader;
