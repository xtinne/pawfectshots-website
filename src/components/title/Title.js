import React from 'react';
import './title.css';

const paw = require('../../assets/images/paw.png');

const Title = props => (
	<div className="title" >
		<h1 className={props.className}><span>{props.title}</span></h1>
		<div className="paw-line"><img src={paw} alt="paw" /></div>
	</div >
);

export default Title;
