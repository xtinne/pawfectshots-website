'use strict'

const functions = require('firebase-functions')
const sendgridMail = require('@sendgrid/mail')
const rp = require('request-promise')

const cors = require('cors')({
  origin: true
})

async function verifyReCaptcha (token) {
  if (!token || Object.keys(token).length === 0) {
    const error = new Error(
      'ReCaptcha token line not provided. Make sure you have a "token" property in your request'
    )
    error.code = 400
    throw error
  }

  const result = await rp({
    uri: 'https://recaptcha.google.com/recaptcha/api/siteverify',
    method: 'POST',
    json: true,
    formData: {
      secret: functions.config().recaptcha.key,
      response: token
    }
  })
  if (!result.success) {
    const error = new Error(
      'Recaptcha request failed.'
    )
    error.code = 400
    throw error
  }
}

function makeMessage (requestBody) {
  if (!requestBody.from) {
    const error = new Error(
      'Email from line not provided. Make sure you have a "from" property in your request'
    )
    error.code = 400
    throw error
  } else if (!requestBody.subject) {
    const error = new Error(
      'Email subject line not provided. Make sure you have a "subject" property in your request'
    )
    error.code = 400
    throw error
  } else if (!requestBody.title) {
    const error = new Error(
      'Email title line not provided. Make sure you have a "title" property in your request'
    )
    error.code = 400
    throw error
  } else if (!requestBody.preheader) {
    const error = new Error(
      'Email preheader line not provided. Make sure you have a "preheader" property in your request'
    )
    error.code = 400
    throw error
  } else if (!requestBody.content) {
    const error = new Error(
      'Email content not provided. Make sure you have a "content" property in your request'
    )
    error.code = 400
    throw error
  }

  const dynamicTemplateData = {
    subject: requestBody.subject,
    title: requestBody.title,
    preheader: requestBody.preheader,
    content: requestBody.content
  }

  return [
    {
      to: requestBody.from,
      from: {
        name: 'Pawfect Shots',
        email: 'hallo@pawfectshots.be'
      },
      templateId: 'd-3422f6d4f092443dbda49a02be8de347',
      dynamic_template_data: dynamicTemplateData
    },
    {
      to: {
        name: 'Pawfect Shots',
        email: 'hallo@pawfectshots.be'
      },
      from: requestBody.from,
      templateId: 'd-3422f6d4f092443dbda49a02be8de347',
      dynamic_template_data: dynamicTemplateData
    }
  ]
}

exports.mail = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    try {
      if (req.method !== 'POST') {
        const error = new Error('Only POST requests are accepted')
        error.code = 405
        throw error
      }

      verifyReCaptcha(req.body.token)

      sendgridMail.setApiKey(functions.config().sendgrid.key)

      const message = makeMessage(req.body)

      sendgridMail.send(message, (error, result) => {
        if (error) {
          const errorMessage = new Error(error)
          errorMessage.code = 400
          throw errorMessage
        } else {
          res.status(200).send({ message: 'OK' })
        }
      })
    } catch (error) {
      res.status(error.code).send({ message: 'ERROR' })
    }
  })
})
